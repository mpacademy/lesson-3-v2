# Steps!
- Clone this repository
- `docker-compose build`
- `docker-compose up -d`
- `docker-compose exec -u 1000 php-fpm composer install`
- If you are using Linux / Docker for Mac / Docker for windows - navigate to localhost:8080
- If you are using Docker Toolbox - you need to add an entry to your /etc/hosts file with an IP to your VM. It can be found by running `docker-machine config`. Afterwards, navigate to <your-docker-host.local>:8080
